# Transaction Service
Transaction Service uses in-memory H2 database and has dependency on transaction-service-interface.

## Installation

#### Install Open JDK

```bash
sudo apt-get update
sudo apt-get install openjdk-8-jdk
```

#### Install Maven

```bash
sudo apt-get install maven
```
#### Install [Transaction-Interface](https://bitbucket.org/raj2331992/transaction-interface/src/master/) Dependency

```bash
git clone https://raj2331992@bitbucket.org/raj2331992/transaction-interface.git
cd transaction-interface
mvn clean install
```

#### Run Transaction-Service

```bash
git clone https://raj2331992@bitbucket.org/raj2331992/transaction-service.git
cd transaction-service/
mvn spring-boot:run
```


## Usage

Once transaction-service starts you can do api requests on default port 8080. You can download the following [Postman Collection](https://www.getpostman.com/collections/fdb1c7dd9b088eb4d487) to test API.


## AWS Deployment
I have deployed the code on an AWS instance. You can use 13.232.250.235:8080 to test the APIs

**Create New Transaction**
```curl
curl -X PUT \
  http://13.232.250.235:8080/transactionservice/transaction/1 \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
	"type":"cars",
	"amount":"200"
}'

```

**Create New Transaction with parent id**
```curl
curl -X PUT \
  http://13.232.250.235:8080/transactionservice/transaction/2 \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
	"parent_id":"1",
	"type":"cars",
	"amount":"200"
}'

```
**Get Transaction**
```curl
curl -X GET \
  http://13.232.250.235:8080/transactionservice/transaction/1 \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' 
```

**Get List of transaction ids by type**
```curl
curl -X GET \
  http://13.232.250.235:8080/transactionservice/types/cars \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' 
```

**Get Transaction sum by parent id**
```curl
curl -X GET \
  http://13.232.250.235:8080/transactionservice/sum/1 \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' 
```
