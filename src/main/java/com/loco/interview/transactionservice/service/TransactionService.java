package com.loco.interview.transactionservice.service;

import com.loco.interview.transactionservice.exceptions.EntityAlreadyExistsException;
import com.loco.interview.transactionservice.exceptions.EntityNotFoundException;
import com.loco.interview.transactionservice.model.Transaction;
import com.loco.interview.transactionservice.repository.TransactionRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import requests.TransactionCreationRequest;
import response.GetTransactionByIdResponse;
import response.GetTransactionListByTypeResponse;
import response.GetTransactionSumByIdResponse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private static Logger logger = LoggerFactory.getLogger(TransactionService.class);

    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void createTransaction(TransactionCreationRequest request, Optional<Long> transactionId){
        if(transactionId.isPresent()) {
            Double amount = request.getAmount();
            String type = request.getType();
            Long parentId = request.getParentId();

            Optional<Transaction> transactionOptional = Optional.empty();
            if(parentId!=null) {
                //Check for cyclic dependencies
                if (parentId.longValue() == transactionId.get().longValue()) {
                    throw new IllegalArgumentException("transaction id and parent id cannot be same");
                }

                //Check if parent transaction exists
                transactionOptional = transactionRepository.findById(parentId);
                if (!transactionOptional.isPresent()) {
                    throw new EntityNotFoundException(String.format("parent transaction with id %d doesn't exists", parentId));
                }
            }

            //Check if transaction already exists
            transactionOptional = transactionRepository.findById(transactionId.get());
            if(transactionOptional.isPresent()){
                throw new EntityAlreadyExistsException(String.format("transaction with id %d already exists",transactionId.get()));
            }

            //Create transaction
            Transaction transaction = Transaction.builder()
                                        .id(transactionId.get())
                                        .amount(amount)
                                        .parentId(parentId)
                                        .type(type)
                                        .status(Transaction.Status.ACTIVE)
                                        .build();
            transactionRepository.save(transaction);
        }else{
            throw new IllegalArgumentException("transaction id is missing");
        }

    }

    public GetTransactionByIdResponse getTransactionById(Optional<Long> transactionId){
        if(transactionId.isPresent()) {
            Optional<Transaction> transactionOptional = transactionRepository.findById(transactionId.get());
            if(transactionOptional.isPresent()){
                Transaction transaction = transactionOptional.get();
                GetTransactionByIdResponse getTransactionByIdResponse = GetTransactionByIdResponse.builder()
                                                                        .amount(transaction.getAmount())
                                                                        .type(transaction.getType())
                                                                        .parentId(transaction.getParentId())
                                                                        .build();
                return getTransactionByIdResponse;
            }else{
                throw new EntityNotFoundException(String.format("transaction with id %d not present",transactionId.get()));
            }

        }else{
            throw new IllegalArgumentException("transaction id is missing");
        }

    }

    public GetTransactionListByTypeResponse getTransactionListByType(Optional<String> type){
        if(type.isPresent()) {
            List<Transaction> transactionsByType = transactionRepository.findByType(type.get());
            if(CollectionUtils.isNotEmpty(transactionsByType)){
                List<Long> transactionsIds = transactionsByType.stream().map(t -> t.getId()).collect(Collectors.toList());

                return GetTransactionListByTypeResponse.builder().transactionIds(transactionsIds).build();
            }else{
                return GetTransactionListByTypeResponse.builder().transactionIds(new ArrayList<>()).build();
            }

        }else{
            throw new IllegalArgumentException("transaction type is missing");
        }
    }

    public GetTransactionSumByIdResponse getTransactionSumById(Optional<Long> transactionId){
        if(transactionId.isPresent()){

            //Check if transaction doesn't exists
            Optional<Transaction> transactionOptional = transactionRepository.findById(transactionId.get());
            if(!transactionOptional.isPresent()){
                throw new EntityNotFoundException(String.format("transaction with id %d doesn't exists",transactionId.get()));
            }


            Double amount = 0.0;
            Queue<Long> children = new LinkedList<>();
            List<Transaction> childTransactions = new ArrayList<>();
            children.add(transactionId.get());
            do {
                childTransactions = transactionRepository.findByParentId(children.remove());
                if (CollectionUtils.isNotEmpty(childTransactions)) {
                    amount += childTransactions.stream()
                            .filter(t -> t.getStatus().equals(Transaction.Status.ACTIVE))
                            .map(t -> {
                                        children.add(t.getId());
                                        return t;
                                      }
                            )
                            .mapToDouble(t -> t.getAmount())
                            .sum();
                }
            }while (children.size()>0);
            return GetTransactionSumByIdResponse.builder().amount(amount).build();
        }else{
            throw new IllegalArgumentException("transaction id is missing");
        }
    }

}
