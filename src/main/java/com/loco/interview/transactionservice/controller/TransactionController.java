package com.loco.interview.transactionservice.controller;

import com.loco.interview.transactionservice.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import requests.TransactionCreationRequest;
import response.Response;
import response.TransactionCreationResponse;

import javax.validation.Valid;
import java.util.Optional;


@RestController
public class TransactionController {

    private static Logger logger = LoggerFactory.getLogger(TransactionController.class);

    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PutMapping("transaction/{transactionId}")
    @ResponseStatus(HttpStatus.OK)
    public Response createTransaction(@PathVariable Long transactionId, @Valid @RequestBody TransactionCreationRequest request){
        transactionService.createTransaction(request, Optional.of(transactionId));
        return TransactionCreationResponse.builder().status("ok").build();
    }

    @GetMapping("transaction/{transactionId}")
    @ResponseStatus(HttpStatus.OK)
    public Response getTransactionById(@PathVariable Long transactionId){
        return transactionService.getTransactionById(Optional.of(transactionId));
    }

    @GetMapping("types/{type}")
    @ResponseStatus(HttpStatus.OK)
    public Response getTransactionsByType(@PathVariable String type){
        return transactionService.getTransactionListByType(Optional.of(type));
    }

    @GetMapping("sum/{transactionId}")
    @ResponseStatus(HttpStatus.OK)
    public Response getTransactionsByType(@PathVariable Long transactionId){
        return transactionService.getTransactionSumById(Optional.of(transactionId));
    }

}
