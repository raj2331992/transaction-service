package com.loco.interview.transactionservice.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
class ApiError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private Integer status;
    private HttpStatus error;
    private String message;

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    ApiError(HttpStatus error) {
        this();
        this.error = error;
        this.status = error.value();
    }

    ApiError(HttpStatus error, Throwable ex) {
        this();
        this.error = error;
        this.status = error.value();
        this.message = "Unexpected Error";
    }

    ApiError(HttpStatus status, String message, Throwable ex) {
        this();
        this.error = status;
        this.status = error.value();
        this.message = message;
    }
}
