package com.loco.interview.transactionservice.repository;

import com.loco.interview.transactionservice.model.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction,Long> {
    List<Transaction> findByType(String type);
    List<Transaction> findByParentId(Long parentId);
}
